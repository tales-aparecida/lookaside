#!/bin/bash

set -euo pipefail

! git check-attr filter -- files/* \
    | grep -v '^files/get-docker.sh' \
    | grep -E '(unspecified|unset)$'
